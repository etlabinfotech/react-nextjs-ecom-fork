export const baseURL = () => {
    return "https://enhancedversion.qfreshonline.com/";
}

export const homeURL = () => {
    return "https://qfreshonline.com/";
}

export const aboutURL = () => {
    return "https://qfreshonline.com/user/about";
}

export const deliveryLocationURL = () => {
    return "https://qfreshonline.com/customer/locations";
}

export const playStoreURL = () => {
    return "https://play.google.com/store/apps/details?id=in.etuwa.qfresh&hl=en";
}

export const AppStoreURL = () => {
    return "https://play.google.com/store/apps/details?id=in.etuwa.qfresh&hl=en";
}

export const appName = () => {
    return "Masskar Online"
}


