import styles from  '../styles/main.module.css'

const CopyRight = () => {
    return (
        <div className={styles.copyRightWrapper}>
            <p  className={styles.copyRightText}>© {new Date().getFullYear()} Masskar. All rights reserved | Developed by Team etuwa</p>
        </div>
    );
}

export default CopyRight