import styles from '../styles/main.module.css'
import Image from 'next/image'
import Link from 'next/link'
import companyLogo from '../public/assets/mAsskar_online.png'
import playStoreLogo from '../public/assets/playstore.png'
import appStoreLogo from '../public/assets/appstore.png'
import { homeURL, aboutURL, deliveryLocationURL, playStoreURL, AppStoreURL } from '../utilities/urls'

const Header = () => {
    return (
        <div className={styles.headerWrapper}>
            <Image src={companyLogo} alt="Masskar online" />
            <Link href={homeURL()}><a className={styles.headerLabels}>Home</a></Link>
            <Link href={aboutURL()}><a className={styles.headerLabels}>About</a></Link>
            <Link href={deliveryLocationURL()}><a className={styles.headerLabels}>Delivery Locations</a></Link>
            <Link href="#"><a className={` ${styles.headerLabels}  ${styles.greenFont}`}> 50343443 </a></Link>
            <Link href="#"><a className={styles.headerLabels}> info@masskaronline.com </a></Link>
            <div className={styles.storeLabels}>
                <Link href={playStoreURL()}><span className={styles.headerLabels}><Image src={playStoreLogo} alt="Play store"/></span></Link>
                <Link href={AppStoreURL()}><span className={styles.headerLabels}><Image src={appStoreLogo} alt="App store" className={styles.headerLabels}/></span></Link>
            </div>
        </div>
    );
}

export default Header;